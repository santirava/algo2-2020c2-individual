#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

class Fecha {
    public:
        Fecha(uint mes, uint dia);
        uint mes();
        uint dia();
        bool operator==(Fecha o);
        void incrementar_dia();

    private:
        int mes_;
        int dia_;
};

Fecha::Fecha(uint mes, uint dia) : mes_(mes), dia_(dia) {}

uint Fecha::mes() {
    return this->mes_;
}

uint Fecha::dia() {
    return this->dia_;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
}

bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}

void Fecha::incrementar_dia() {
    if (this->dia() < dias_en_mes(this->mes())) {
        this->dia_++;
    } else {
        if (this->mes() < 12) {
            this->mes_ = this->mes() + 1;
            this->dia_ = 1;
        } else {
            this->mes_ = 1;
            this->dia_ = 1;
        }
    }
}

class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario o);
    bool operator<(Horario o);

private:
    int hora_;
    int min_;
};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min) {}

uint Horario::hora() {
    return this->hora_;
}

uint Horario::min() {
    return this->min_;
}

bool Horario::operator==(Horario o) {
    bool igual_hora = o.hora() == this->hora();
    bool igual_min = o.min() == this->min();
    return igual_hora && igual_min;
}

bool Horario::operator<(Horario o) {
    bool res = false;
    if (this->hora() < o.hora()) {
        res = true;
    } else if (this->hora() == o.hora() && this->min() < o.min()) {
        res = true;
    }
    return res;
}


ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
}

class Recordatorio {
    public:
        Recordatorio(Fecha f, Horario h, string mensaje);
        Fecha fecha();
        Horario horario();
        string mensaje();
    private:
        string mensaje_;
        Fecha fecha_;
        Horario horario_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string mensaje) : fecha_(f), horario_(h), mensaje_(mensaje) {}

Fecha Recordatorio::fecha() {
    return this->fecha_;
}

Horario Recordatorio::horario() {
    return this->horario_;
}

string Recordatorio::mensaje() {
    return this->mensaje_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
        list<Recordatorio> recordatorios_;
        Fecha hoy_;
};

Agenda::Agenda(Fecha fecha_inicial) : recordatorios_(), hoy_(fecha_inicial) {}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    this->recordatorios_.push_back(rec);
}

void Agenda::incrementar_dia() {
    this->hoy_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> reco_de_hoy;
    vector<Recordatorio> vec_rec;
    for (auto r : this->recordatorios_) {
        if (r.fecha() == this->hoy()) {
            vec_rec.push_back(r);
        }
    }

    for (int i = 0; i < vec_rec.size(); ++i) {
        for (int j = i+1; j < vec_rec.size(); ++j) {
            if (vec_rec[j].horario() < vec_rec[i].horario()) {
                Recordatorio aux = vec_rec[i];
                vec_rec[i] = vec_rec[j];
                vec_rec[j] = aux;
            }
        }
    }

    for (int i = 0; i < vec_rec.size(); ++i) {
        reco_de_hoy.push_back(vec_rec[i]);
    }
    return reco_de_hoy;
}

Fecha Agenda::hoy() {
    return this->hoy_;
}

ostream& operator<<(ostream& os, Agenda a) {
    list<Recordatorio> reco_de_hoy = a.recordatorios_de_hoy();
    os << a.hoy() << "\n" << "=====" << endl;
    for(auto r : reco_de_hoy)
    {
        os << r.mensaje() <<" @ "<< r.fecha() << " " << r.horario() << "\n";
    }
    return os;
}


