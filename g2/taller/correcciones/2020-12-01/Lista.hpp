#include "Lista.h"

Lista::Nodo::Nodo(const int &elem): data(elem), next(nullptr), prev(nullptr) {}

Lista::Lista(): _inicio(nullptr), _fin(nullptr), _longitud(0) {
    // Completar
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    this->limpiar_lista_();
}

Lista& Lista::operator=(const Lista& aCopiar) {
    this->limpiar_lista_();
    this->_longitud = 0;
    for (int i = 0; i < aCopiar.longitud(); ++i) {
        this->agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    if(this->_inicio == nullptr){
        this->_inicio = new Nodo(elem);
        this->_fin = this->_inicio;
    } else {
        this->_inicio->prev = new Nodo(elem);
        this->_inicio->prev->next = this->_inicio;
        this->_inicio = this->_inicio->prev;
    }
    this->_longitud++;
}

void Lista::agregarAtras(const int& elem) {
    if(this->_inicio == nullptr){
        this->_inicio = new Nodo(elem);
        this->_fin = this->_inicio;
    } else {
        this->_fin->next = new Nodo(elem);
        this->_fin->next->prev = this->_fin;
        this->_fin = this->_fin->next;
    }
    this->_longitud++;
}

void Lista::eliminar(Nat i) {
    if(i >=0  && i < this->_longitud){
        Nodo* temp;
        if(_longitud == 1){
            temp = this->_inicio;
            this->_inicio = nullptr;
            this->_fin = nullptr;
        } else if(i==0){
            temp = this->_inicio;
            this->_inicio = temp->next;
            this->_inicio->prev = nullptr;
        }else if(i == this->_longitud - 1){
            temp = this->_fin;
            this->_fin = temp->prev;
            this->_fin->next = nullptr;
        } else if(i<this->_longitud/2){
            int j = 0;
            temp = this->_inicio;
            while(j!=i){
                temp= temp->next;
                j++;
            }
            temp->prev->next = temp->next;
            temp->next->prev = temp->prev;
        } else {
            int j = this->_longitud - 1;
            temp = this->_fin;
            while(j!=i){
                temp= temp->prev;
                j--;
            }
            temp->prev->next = temp->next;
            temp->next->prev = temp->prev;
        }

        delete temp;
        this->_longitud--;
    }
}

int Lista::longitud() const {
    return this->_longitud;
}

const int& Lista::iesimo(Nat i) const {
    int j = 0;
    Nodo* temp = this->_inicio;
    while(temp != nullptr && j!=i){
        temp= temp->next;
        j++;
    }
    return temp->data;
}

int& Lista::iesimo(Nat i) {
    int j = 0;
    Nodo* temp = this->_inicio;
    while(temp != nullptr && j!=i){
        temp= temp->next;
        j++;
    }
    return temp->data;
}

void Lista::mostrar(ostream& o) {
    // Completar
}

void Lista::limpiar_lista_() {
    Nodo* temp = this->_inicio;
    while(temp != nullptr){
        temp= temp->next;
        delete(this->_inicio);
        this->_inicio = temp;
    }
}