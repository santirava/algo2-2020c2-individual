#ifndef STRING_MAP_H_
#define STRING_MAP_H_

#include <string>

using namespace std;

template<typename T>
class string_map {
public:
    string_map();
    string_map(const string_map<T>& aCopiar);
    string_map& operator=(const string_map& d);
    ~string_map();
    void insert(const pair<string, T>&);
    int count(const string &key) const;
    const T& at(const string& key) const;
    T& at(const string& key);
    void erase(const string& key);
    int size() const;
    bool empty() const;
    T &operator[](const string &key);

private:
    struct Nodo {
        Nodo();
        Nodo(const Nodo* aCopiar);
        vector<Nodo*> siguientes;
        T* definicion;
    };
    Nodo* raiz;
    int _size;
    void _insert(const pair<string, T>& data, Nodo* actualNode);
    T& _at(string clave, Nodo* actualNode);
    int _count(const string& clave, Nodo* actualNode) const;
    void _vaciar(string_map::Nodo* node);
};

#include "string_map.hpp"

#endif // STRING_MAP_H_